﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using System.Threading;
using System.Runtime.InteropServices;//引用命名空间
using System.Windows.Forms.DataVisualization.Charting;

using System.Collections;
using ZedGraph;

namespace SerialDemo
{
    public partial class SerialForm : Form
    {
        Form1 noframe = new Form1();
        ComSet comset = new ComSet();
        private Queue<double> dataQueue = new Queue<double>(256);
       
        public static string strPortName = "";
        public static string strBaudRate = "";
        public static string strDataBits = "";
        public static string strStopBits = "";
        bool flagstartup = false;

        #region "配置文件声明变量"
        /// <summary>
        /// 写入INI文件
        /// </summary>
        /// <param name="section">节点名称[如[TypeName]]</param>
        /// <param name="key">键</param>
        /// <param name="val">值</param>
        /// <param name="filepath">文件路径</param>
        /// <returns></returns>
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string val, string filepath);
        /// <summary>
        /// 读取INI文件
        /// </summary>
        /// <param name="section">节点名称</param>
        /// <param name="key">键</param>
        /// <param name="def">值</param>
        /// <param name="retval">stringbulider对象</param>
        /// <param name="size">字节大小</param>
        /// <param name="filePath">文件路径</param>
        /// <returns></returns>
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string def, StringBuilder retval, int size, string filePath);

        private string strFilePath = Application.StartupPath + "\\FileConfig.ini";//获取INI文件路径

        #endregion

        /// <summary>
        /// 自定义读取INI文件中的内容方法
        /// </summary>
        /// <param name="Section">键</param>
        /// <param name="key">值</param>
        /// <returns></returns>
        private string ContentValue(string Section, string key)
        {

            StringBuilder temp = new StringBuilder(1024);
            GetPrivateProfileString(Section, key, "", temp, 1024, strFilePath);
            return temp.ToString();
        }

        public SerialForm()
        {
            InitializeComponent();
            //noframe.ShowDialog();
            comset.ShowDialog();
            //Roll
            ERollP.Text = ContentValue("ROLLPID", "ERollP");
            ERollI.Text = ContentValue("ROLLPID", "ERollI");
            ERollD.Text = ContentValue("ROLLPID", "ERollD");
            IRollP.Text = ContentValue("ROLLPID", "IRollP");
            IRollI.Text = ContentValue("ROLLPID", "IRollI");
            IRollD.Text = ContentValue("ROLLPID", "IRollD");
            flagstartup = true;
            cbx_Ymax.SelectedIndex = 2;
            InitChart();
        }

        public float StrToFloat(object FloatString)
        {
            float result;
            if (FloatString != null)
            {
                if (float.TryParse(FloatString.ToString(), out result))
                    return result;
                else
                {
                    return (float)0.00;
                }
            }
            else
            {
                return (float)0.00;
            }
        }

        #region "串口接收"
        Int16 nCaseCount = 1;
        Int16 nRevBuffCount = 0;
        bool Rev_Ok_Flag = false;
        byte[] RecieveBuf = new byte[2060];

        void Rev_Deal(byte byteRev)
        {
            switch (nCaseCount)
            {
                case 1:
                    {
                        if (byteRev == 'G')
                        {
                            RecieveBuf[nRevBuffCount] = byteRev;
                            nCaseCount++;
                            nRevBuffCount++;
                        }
                        else
                        {
                            nCaseCount = 1;
                            nRevBuffCount = 0;
                        }
                    } break;
                case 2:
                    {
                        if (byteRev == 'T')
                        {
                            RecieveBuf[nRevBuffCount] = byteRev;
                            nCaseCount++;
                            nRevBuffCount++;
                        }
                        else
                        {
                            nCaseCount = 1;
                            nRevBuffCount = 0;
                        }
                    } break;
                default:
                    {
                        RecieveBuf[nRevBuffCount] = byteRev;
                        if (nCaseCount == 2052)//RecieveBuf[2])
                        {
                            nCaseCount = 1;
                            nRevBuffCount = 0;
                            Rev_Ok_Flag = true;
                        }
                        else
                        {
                            nCaseCount++;
                            nRevBuffCount++;
                        }
                    } break;
            }
        }

        float curve1_show = new float();
        private void receive_send()
        {
            //这里是用来收发数据
            try
            {
                while (serialPort1.IsOpen)
                {
                    Rev_Deal((byte)serialPort1.ReadByte());

                    if (Rev_Ok_Flag)
                    {
                        Rev_Ok_Flag = false;
                        for (UInt16 i = 0; i < 512; i++)
                        {
                            curve1_show = BitConverter.ToSingle(RecieveBuf, 4+4*i);
                            if (dataQueue.Count > 512)
                            {
                                //先出列
                                dataQueue.Dequeue();
                            }
                            dataQueue.Enqueue(curve1_show);
                        }
                        Invoke(new MethodInvoker(delegate()
                        {
                            this.chart1.Series[0].Points.Clear();
                            for (int i = 0; i < dataQueue.Count; i++)
                            {
                                this.chart1.Series[0].Points.AddXY(i + 1, dataQueue.ElementAt(i));
                            }
                        }));
                   }
                }
            }catch{}

        }
        #endregion

        //发送BtnSendInfo
        private void btnSend_Click(object sender, EventArgs e)
        {
            float rollp,rolli,rolld;
            if (serialPort1.IsOpen)
            {
                byte[] SendBuf = new byte[100];
                SendBuf[0] = 0xA5;
                SendBuf[1] = 0x5A;
                SendBuf[2] = 0x02;
                //横滚PID
                rollp = StrToFloat(ERollP.Text);
                (BitConverter.GetBytes(rollp)).CopyTo(SendBuf, 3);//float to byte
                rolli = StrToFloat(ERollI.Text);
                (BitConverter.GetBytes(rolli)).CopyTo(SendBuf, 7);//float to byte
                rolld = StrToFloat(ERollD.Text);
                (BitConverter.GetBytes(rolld)).CopyTo(SendBuf, 11);//float to byte

                serialPort1.Write(SendBuf, 0, 15);
            }
            else
            {
                MessageBox.Show("串口没有打开，请打开串口!");
            }
        }
        private void Menu_Ctl_Port_Click(object sender, EventArgs e)
        {
            if (!serialPort1.IsOpen)
            {
                Menu_Ctl_Port.Text = "关闭串口";
                Status_Com.Text = "串口" + strPortName + "已打开";

                serialPort1.PortName = strPortName;
                serialPort1.BaudRate = int.Parse(strBaudRate);
                serialPort1.DataBits = int.Parse(strDataBits);
                serialPort1.StopBits = (StopBits)int.Parse(strStopBits);
                //打开
                serialPort1.Open();
                Thread CollectThread = new Thread(receive_send);
                CollectThread.Start();
            }
            else
            {
                Menu_Ctl_Port.Text = "打开串口";
                Status_Com.Text = "串口关闭";
                serialPort1.Close();
            }
        }

        //清空
        private void btnClear_Click(object sender, EventArgs e)
        {
            if ((sender as Button) ==btnClearRead)
            {
                txtRecieve.Clear();
            }
        }

        private void SerialForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            serialPort1.Close();
        }

        private void Menu_Set_Comset_Click(object sender, EventArgs e)
        {
            comset.ShowDialog();
        }

        private void Menu_About_Click(object sender, EventArgs e)
        {
            About about = new About();
            about.Show();
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////绘图区域////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region "声明变量"
        /// <summary>
        /// 初始化图表
        /// </summary>
        private void InitChart()
        {
            //定义图表区域
            this.chart1.ChartAreas.Clear();
            ChartArea chartArea1 = new ChartArea("C1");
            this.chart1.ChartAreas.Add(chartArea1);
            //定义存储和显示点的容器
            this.chart1.Series.Clear();
            Series series1 = new Series("S1");
            series1.ChartArea = "C1";
            this.chart1.Series.Add(series1);
            //设置图表显示样式
            this.chart1.ChartAreas[0].AxisY.Minimum = 0;
            this.chart1.ChartAreas[0].AxisY.Maximum = double.Parse(cbx_Ymax.Text);//1;
            this.chart1.ChartAreas[0].AxisX.Interval = 10;
            this.chart1.ChartAreas[0].AxisX.MajorGrid.LineColor = System.Drawing.Color.Silver;
            this.chart1.ChartAreas[0].AxisY.MajorGrid.LineColor = System.Drawing.Color.Silver;
            //设置标题
            this.chart1.Titles.Clear();
            this.chart1.Titles.Add("S01");
            this.chart1.Titles[0].Text = "XXX显示";
            this.chart1.Titles[0].ForeColor = Color.RoyalBlue;
            this.chart1.Titles[0].Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            //设置图表显示样式
            this.chart1.Series[0].Color = Color.Red;
            this.chart1.Titles[0].Text = "波形图";
            this.chart1.Series[0].ChartType = SeriesChartType.Spline;
            this.chart1.Series[0].Points.Clear();
        }


        #endregion
        private void ROLLPID_ValueChanged(object sender, EventArgs e)
        {
            float ep, ei, ed, ip, ii, id;
            if (flagstartup)
            {

                try
                {
                    //根据INI文件名设置要写入INI文件的节点名称
                    //此处的节点名称完全可以根据实际需要进行配置
                    WritePrivateProfileString("ROLLPID", "ERollP", ERollP.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("ROLLPID", "ERollI", ERollI.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("ROLLPID", "ERollD", ERollD.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("ROLLPID", "IRollP", IRollP.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("ROLLPID", "IRollI", IRollI.Value.ToString().Trim(), strFilePath);
                    WritePrivateProfileString("ROLLPID", "IRollD", IRollD.Value.ToString().Trim(), strFilePath);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message.ToString());
                }
                if (serialPort1.IsOpen)
                {
                    byte[] SendBuf = new byte[100];
                    SendBuf[0] = 0xA5;
                    SendBuf[1] = 0x5A;
                    SendBuf[2] = 0x02;
                    //横滚PID
                    ep = StrToFloat(ERollP.Value.ToString());
                    (BitConverter.GetBytes(ep)).CopyTo(SendBuf, 3);//float to byte
                    ei = StrToFloat(ERollI.Value.ToString());
                    (BitConverter.GetBytes(ei)).CopyTo(SendBuf, 7);//float to byte
                    ed = StrToFloat(ERollD.Value.ToString());
                    (BitConverter.GetBytes(ed)).CopyTo(SendBuf, 11);//float to byte
                    //横滚PID
                    ip = StrToFloat(IRollP.Value.ToString());
                    (BitConverter.GetBytes(ip)).CopyTo(SendBuf, 15);//float to byte
                    ii = StrToFloat(IRollI.Value.ToString());
                    (BitConverter.GetBytes(ii)).CopyTo(SendBuf, 19);//float to byte
                    id = StrToFloat(IRollD.Value.ToString());
                    (BitConverter.GetBytes(id)).CopyTo(SendBuf, 23);//float to byte

                    serialPort1.Write(SendBuf, 0, 27);
                }
                else
                {
                    if (flagstartup)
                        MessageBox.Show("串口没有打开，请打开串口!");
                }
            }
        }

        private void cbx_Ymax_SelectedIndexChanged(object sender, EventArgs e)
        {
            InitChart();
        }

    }
}