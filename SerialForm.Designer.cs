﻿namespace SerialDemo
{
    partial class SerialForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SerialForm));
            this.txtRecieve = new System.Windows.Forms.TextBox();
            this.btnClearRead = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.Menu_Set = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Set_Comset = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_Ctl_Port = new System.Windows.Forms.ToolStripMenuItem();
            this.Menu_About = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.Status_Com = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.Status_Lang = new System.Windows.Forms.ToolStripStatusLabel();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cbx_Ymax = new System.Windows.Forms.ComboBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.IRollP = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.IRollD = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.IRollI = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.ERollP = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.ERollD = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.ERollI = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IRollP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IRollD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IRollI)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERollP)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERollD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERollI)).BeginInit();
            this.SuspendLayout();
            // 
            // txtRecieve
            // 
            this.txtRecieve.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRecieve.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRecieve.Location = new System.Drawing.Point(3, 17);
            this.txtRecieve.Multiline = true;
            this.txtRecieve.Name = "txtRecieve";
            this.txtRecieve.ReadOnly = true;
            this.txtRecieve.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtRecieve.Size = new System.Drawing.Size(860, 64);
            this.txtRecieve.TabIndex = 0;
            // 
            // btnClearRead
            // 
            this.btnClearRead.Dock = System.Windows.Forms.DockStyle.Right;
            this.btnClearRead.Location = new System.Drawing.Point(863, 17);
            this.btnClearRead.Name = "btnClearRead";
            this.btnClearRead.Size = new System.Drawing.Size(70, 64);
            this.btnClearRead.TabIndex = 19;
            this.btnClearRead.Text = "清空";
            this.btnClearRead.UseVisualStyleBackColor = true;
            this.btnClearRead.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.txtRecieve);
            this.groupBox3.Controls.Add(this.btnClearRead);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.groupBox3.Location = new System.Drawing.Point(0, 676);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(936, 84);
            this.groupBox3.TabIndex = 21;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "接收数据";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Set,
            this.Menu_Ctl_Port,
            this.Menu_About});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1126, 25);
            this.menuStrip1.TabIndex = 22;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // Menu_Set
            // 
            this.Menu_Set.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Menu_Set_Comset});
            this.Menu_Set.Image = global::SerialDemo.Properties.Resources.Customize;
            this.Menu_Set.Name = "Menu_Set";
            this.Menu_Set.Size = new System.Drawing.Size(74, 21);
            this.Menu_Set.Text = "设置(&F)";
            // 
            // Menu_Set_Comset
            // 
            this.Menu_Set_Comset.Image = global::SerialDemo.Properties.Resources.USB;
            this.Menu_Set_Comset.Name = "Menu_Set_Comset";
            this.Menu_Set_Comset.Size = new System.Drawing.Size(140, 22);
            this.Menu_Set_Comset.Text = "端口设置(&C)";
            this.Menu_Set_Comset.Click += new System.EventHandler(this.Menu_Set_Comset_Click);
            // 
            // Menu_Ctl_Port
            // 
            this.Menu_Ctl_Port.Name = "Menu_Ctl_Port";
            this.Menu_Ctl_Port.Size = new System.Drawing.Size(68, 21);
            this.Menu_Ctl_Port.Text = "打开串口";
            this.Menu_Ctl_Port.Click += new System.EventHandler(this.Menu_Ctl_Port_Click);
            // 
            // Menu_About
            // 
            this.Menu_About.Image = global::SerialDemo.Properties.Resources.Get_Info;
            this.Menu_About.Name = "Menu_About";
            this.Menu_About.Size = new System.Drawing.Size(76, 21);
            this.Menu_About.Text = "关于(&A)";
            this.Menu_About.Click += new System.EventHandler(this.Menu_About_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Status_Com,
            this.toolStripStatusLabel1,
            this.Status_Lang});
            this.statusStrip1.Location = new System.Drawing.Point(0, 760);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(936, 22);
            this.statusStrip1.TabIndex = 23;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // Status_Com
            // 
            this.Status_Com.Name = "Status_Com";
            this.Status_Com.Size = new System.Drawing.Size(56, 17);
            this.Status_Com.Text = "串口关闭";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(801, 17);
            this.toolStripStatusLabel1.Spring = true;
            this.toolStripStatusLabel1.Text = "           ";
            // 
            // Status_Lang
            // 
            this.Status_Lang.Name = "Status_Lang";
            this.Status_Lang.Size = new System.Drawing.Size(64, 17);
            this.Status_Lang.Text = "中文(中国)";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.cbx_Ymax);
            this.groupBox6.Controls.Add(this.chart1);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(922, 619);
            this.groupBox6.TabIndex = 37;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "实时曲线";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 12);
            this.label1.TabIndex = 4;
            this.label1.Text = "YMax";
            // 
            // cbx_Ymax
            // 
            this.cbx_Ymax.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbx_Ymax.FormattingEnabled = true;
            this.cbx_Ymax.Items.AddRange(new object[] {
            "1",
            "2",
            "4",
            "8",
            "16",
            "32",
            "64",
            "128"});
            this.cbx_Ymax.Location = new System.Drawing.Point(60, 19);
            this.cbx_Ymax.Name = "cbx_Ymax";
            this.cbx_Ymax.Size = new System.Drawing.Size(82, 20);
            this.cbx_Ymax.TabIndex = 3;
            this.cbx_Ymax.SelectedIndexChanged += new System.EventHandler(this.cbx_Ymax_SelectedIndexChanged);
            // 
            // chart1
            // 
            this.chart1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chart1.Location = new System.Drawing.Point(3, 17);
            this.chart1.Name = "chart1";
            this.chart1.Size = new System.Drawing.Size(916, 599);
            this.chart1.TabIndex = 2;
            this.chart1.Text = "chart1";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 25);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(936, 651);
            this.tabControl1.TabIndex = 41;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox6);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(928, 625);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "图形观测";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.textBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(928, 625);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "数据观测";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(922, 619);
            this.textBox1.TabIndex = 0;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.groupBox1);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupBox5.Location = new System.Drawing.Point(936, 25);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(190, 757);
            this.groupBox5.TabIndex = 42;
            this.groupBox5.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.IRollP);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.IRollD);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.IRollI);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.ERollP);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.ERollD);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.ERollI);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Location = new System.Drawing.Point(30, 25);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(126, 196);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "PID(Roll)";
            // 
            // IRollP
            // 
            this.IRollP.DecimalPlaces = 2;
            this.IRollP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IRollP.Location = new System.Drawing.Point(42, 107);
            this.IRollP.Name = "IRollP";
            this.IRollP.Size = new System.Drawing.Size(64, 21);
            this.IRollP.TabIndex = 37;
            this.IRollP.ValueChanged += new System.EventHandler(this.ROLLPID_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(17, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 36;
            this.label4.Text = "内P";
            // 
            // IRollD
            // 
            this.IRollD.DecimalPlaces = 2;
            this.IRollD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IRollD.Location = new System.Drawing.Point(42, 165);
            this.IRollD.Name = "IRollD";
            this.IRollD.Size = new System.Drawing.Size(64, 21);
            this.IRollD.TabIndex = 39;
            this.IRollD.ValueChanged += new System.EventHandler(this.ROLLPID_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(17, 167);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(23, 12);
            this.label5.TabIndex = 35;
            this.label5.Text = "内D";
            // 
            // IRollI
            // 
            this.IRollI.DecimalPlaces = 2;
            this.IRollI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.IRollI.Location = new System.Drawing.Point(42, 136);
            this.IRollI.Name = "IRollI";
            this.IRollI.Size = new System.Drawing.Size(64, 21);
            this.IRollI.TabIndex = 38;
            this.IRollI.ValueChanged += new System.EventHandler(this.ROLLPID_ValueChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(17, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(23, 12);
            this.label6.TabIndex = 34;
            this.label6.Text = "内I";
            // 
            // ERollP
            // 
            this.ERollP.DecimalPlaces = 2;
            this.ERollP.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ERollP.Location = new System.Drawing.Point(42, 20);
            this.ERollP.Name = "ERollP";
            this.ERollP.Size = new System.Drawing.Size(64, 21);
            this.ERollP.TabIndex = 31;
            this.ERollP.ValueChanged += new System.EventHandler(this.ROLLPID_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(17, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(23, 12);
            this.label9.TabIndex = 30;
            this.label9.Text = "外P";
            // 
            // ERollD
            // 
            this.ERollD.DecimalPlaces = 2;
            this.ERollD.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ERollD.Location = new System.Drawing.Point(42, 78);
            this.ERollD.Name = "ERollD";
            this.ERollD.Size = new System.Drawing.Size(64, 21);
            this.ERollD.TabIndex = 33;
            this.ERollD.ValueChanged += new System.EventHandler(this.ROLLPID_ValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(17, 80);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(23, 12);
            this.label8.TabIndex = 29;
            this.label8.Text = "外D";
            // 
            // ERollI
            // 
            this.ERollI.DecimalPlaces = 2;
            this.ERollI.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.ERollI.Location = new System.Drawing.Point(42, 49);
            this.ERollI.Name = "ERollI";
            this.ERollI.Size = new System.Drawing.Size(64, 21);
            this.ERollI.TabIndex = 32;
            this.ERollI.ValueChanged += new System.EventHandler(this.ROLLPID_ValueChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(17, 51);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(23, 12);
            this.label7.TabIndex = 28;
            this.label7.Text = "外I";
            // 
            // SerialForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1126, 782);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SerialForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "云台调试助手V1.0---陈梦洋";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SerialForm_FormClosing);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.IRollP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IRollD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IRollI)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERollP)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERollD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ERollI)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtRecieve;
        private System.Windows.Forms.Button btnClearRead;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Set;
        private System.Windows.Forms.ToolStripMenuItem Menu_Set_Comset;
        private System.Windows.Forms.ToolStripMenuItem Menu_About;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel Status_Com;
        private System.Windows.Forms.ToolStripStatusLabel Status_Lang;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ToolStripMenuItem Menu_Ctl_Port;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown IRollP;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown IRollD;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown IRollI;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown ERollP;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown ERollD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown ERollI;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cbx_Ymax;
        private System.Windows.Forms.Label label1;
    }
}

