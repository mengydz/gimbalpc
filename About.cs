﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace SerialDemo
{
    public partial class About : Form
    {
        public About()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 禁用窗体移动等操作
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            if (m.Msg == 0xa3 || (m.Msg == 0x00A1 && m.WParam.ToInt32() == 2) || m.WParam.ToInt32() == 0xF010)
            {
                return;
            }
            base.WndProc(ref m);
        }
    }
}
